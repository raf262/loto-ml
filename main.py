import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression


def open_csv_files(csv_files):
    results = []
    for file in csv_files:
        results.append(pd.read_csv(
            filepath_or_buffer=file['name'],
            header=file['header'],
            sep=file['separator'],
            usecols=file['columns']
        ))
        if 'rename' in file:
            results[-1] = results[-1].rename(columns=file['rename'])
    return pd.concat(results, axis=0)


def column_to_numpy_array(dataframe):
    df_sub = dataframe.to_numpy()
    y = []
    x = []
    for index, value in enumerate(df_sub):
        y.append([value])
        x.append([index])
    return {'y': y, 'x': x}


def predict_for_array(array_numpy, column_name):
    index_prediction = len(array_numpy['x'])
    to_predict_x = [index_prediction]
    to_predict_x = np.array(to_predict_x).reshape(-1, 1)
    regsr = LinearRegression()
    regsr.fit(array_numpy['x'], array_numpy['y'])
    predicted_y = regsr.predict(to_predict_x)
    m = regsr.coef_
    c = regsr.intercept_
    print("Predicted y:\n", predicted_y)
    print("slope (m): ", m)
    print("y-intercept (c): ", c)
    draw_graphic(
        x=array_numpy['x'],
        y=array_numpy['y'],
        m=m,
        c=c,
        to_predict_x=to_predict_x,
        column_name=column_name
    )
    return round(predicted_y[0][0])


def draw_graphic(x, y, m, c, to_predict_x, column_name):
    plt.title('Predict the next numbers in a given sequence')
    plt.xlabel('X')
    plt.ylabel('Numbers')
    plt.scatter(x, y, color="blue")
    new_y = [m * i + c for i in np.append(x, to_predict_x)]
    new_y = np.array(new_y).reshape(-1, 1)
    plt.plot(np.append(x, to_predict_x), new_y, color="red")
    plt.savefig('result/'+column_name+'.png')


if __name__ == '__main__':
    array_files = [
        {
            'name': 'data/loto.csv',
            'header': 0,
            'separator': ';',
            'columns': ['boule_1', 'boule_2', 'boule_3', 'boule_4', 'boule_5', 'boule_complementaire'],
            'rename': {"boule_complementaire": "numero_chance"}
        },
        {
            'name': 'data/loto2017.csv',
            'header': 0,
            'separator': ';',
            'columns': ['boule_1', 'boule_2', 'boule_3', 'boule_4', 'boule_5', 'numero_chance']
        },
        {
            'name': 'data/loto_201911.csv',
            'header': 0,
            'separator': ';',
            'columns': ['boule_1', 'boule_2', 'boule_3', 'boule_4', 'boule_5', 'numero_chance']
        },
        {
            'name': 'data/nouveau_loto.csv',
            'header': 0,
            'separator': ';',
            'columns': ['boule_1', 'boule_2', 'boule_3', 'boule_4', 'boule_5', 'numero_chance']
        }
    ]
    df = open_csv_files(csv_files=array_files)
    predictions = []
    for column in df:
        numpy_column = column_to_numpy_array(df[column])
        predicted_value = predict_for_array(numpy_column, column)
        predictions.append({column: predicted_value})
    print(predictions)
    exit(1)




